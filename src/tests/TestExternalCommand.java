/**
 * Class to test if running external commands from Java is possible.
 */
package tests;

import java.io.*;

/**
 * @author suvam
 *
 */
public class TestExternalCommand {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		Process p;
		String command = "ls";
		p = Runtime.getRuntime().exec(command);
		p.waitFor();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line = "";
		while((line=br.readLine())!=null)
			System.out.println(line);
		
		
		System.out.println("\nJava program terminated.");
	}

}
