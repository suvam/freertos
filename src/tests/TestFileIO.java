/**
 * 
 */
package tests;

/**
 * @author suvam
 *
 * This class tests if the reading and writing of template.pml is operating correctly. There is an issue that FileGeneratorScript.java fails to read template.pml
 */

import java.io.*;

public class TestFileIO {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String relativeAddress = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		try
		{
			System.out.println("\nPlease enter path: ");
			relativeAddress = br.readLine();
			relativeAddress = relativeAddress.trim();
		}
		catch(IOException e)
		{
			System.out.println("\nAn IO Error occurred");
		}
		
		try
		{
			BufferedReader fileReader = new BufferedReader(new FileReader(relativeAddress));
			String line; 
			while( (line = fileReader.readLine()) != null)
				System.out.println(line);
		}
		catch(FileNotFoundException e)
		{
			System.out.println("\nCould not load file: " + relativeAddress);
		}
		catch(IOException e)
		{
			System.out.println("\nAn exception occurred while reading the file");
		}

	}

}
