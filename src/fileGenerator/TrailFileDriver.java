/**
 * Driver thread to make the parallelize the processing of the trail files.
 */
package fileGenerator;

import java.io.*;

/**
 * @author suvam
 *
 */
public class TrailFileDriver {

	/**
	 * @param args
	 * args[0]: full path to the files directory, eg: /media/suvam/dataDrive/workspace/files/
	 * args[1]: number of sub-directories inside files
	 * args[2]: path to store the log file
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		if(args.length < 3)
		{
			System.out.println("\nInvalid number of arguments");
			System.exit(1);
		}
		
		String directory = args[0];
		directory.trim();
		
		String logFile = args[2];
		logFile.trim();
		BufferedWriter logWriter = new BufferedWriter(new FileWriter(new File(logFile)));
		
		int numSubDirectories = Integer.parseInt(args[1]);
		
		TrailFileWorkerThread workers[] = new TrailFileWorkerThread[numSubDirectories];	// factory of worker threads
		long startTime = System.nanoTime();	// Log the start time
		for(int i=0; i<numSubDirectories; i++)
		{
			workers[i] = new TrailFileWorkerThread(directory, String.valueOf(i), logWriter);
			workers[i].start();	// Start processing the i-th subdirectory
		}
		
		for(int i = 0; i<numSubDirectories; i++)
		{
			try
			{
				workers[i].join();
			}
			catch(InterruptedException e)
			{
				System.out.println("\nError while joining worker thread " + i);
			}
		}
		
		long processingTime = System.nanoTime() - startTime;
		
		try
		{
			logWriter.write("Total processing time: " + processingTime);
		}
		catch(IOException e)
		{
			System.out.println("\nError while writing to log file");
		}
		
	}

}
