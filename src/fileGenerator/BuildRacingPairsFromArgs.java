/**
 * 
 */
package fileGenerator;

/**
 * @author suvam
 *
 * Build the racing pairs from the given command line arguments.
 * A script will continually feed the necessary arguments. This program is needed as the 
 * BuildRacingPairs is running out of heap memory.
 * 
 * args[0] : Complete path to the 'files' directory
 * args[1] : Complete path (including file name) to which the unique racing pairs will be written
 * args[2] : Start processing from this directory
 * args[3]:  Stop processing at this directory
 */

import java.util.*;
import java.io.*;

public class BuildRacingPairsFromArgs {
	
	
	/*
	 * Method to check if the trail file is indeed due to an assertion violation.
	 * This is a valid error trace if it contains an assertion violation
	 */
	public static int checkAssertionFile(String input) throws FileNotFoundException, IOException
	{
		BufferedReader br = new BufferedReader(new FileReader(new File(input)));
		String line;
		
		while((line = br.readLine())!=null)
		{
			if(line.contains("Error: assertion violated"))
			{
				br.close();
				return 1;
			}
				
		}
		br.close();
		return 0;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		long startTime = System.nanoTime();
		int validAssertionFile = 0;

		if(args.length != 4)
		{
			System.out.println("\nIncorrect number of arguments. Please retry.");
			System.exit(1);
		}
		else
		{
			String inputPath = args[0];
			String outPath = args[1];
			
			Hashtable<Key, Value> racingPairs = new Hashtable<Key, Value>();
			
			outPath = outPath.trim();
			File outf = new File(outPath);
			BufferedWriter outbr = new BufferedWriter(new FileWriter(outf));
			
			int start = 1;
			int end = 1;
			start = Integer.parseInt(args[2]);
			end = Integer.parseInt(args[3]);
			
			//System.out.println("\nProcessing Directory: " + start);
			
			for(int i=start; i <= end; i++)
			{
				System.out.println("\nAnalyzing directory: " + i);
				// System.out.println(inputPath + i + "/");
				File f = new File(inputPath + i + "/");
				
				int trailFilesCount = f.listFiles(new FilenameFilter() { 
		   	         public boolean accept(File dir, String filename)
		             { return (filename.endsWith(".txt") && filename.contains("trail"))	; }
					} ).length;
			
				for(int j = 1;j<=trailFilesCount;j++){
					testReadReverse t = new testReadReverse();
					
					//System.out.println("\nNow processing file: " + inputPath + i + "/" + "log3trail" + j + ".txt");
					String filePath = inputPath + i + "/" + "log3trail" + j + ".txt";
					validAssertionFile = checkAssertionFile(filePath);
					
					if(validAssertionFile == 0)
						continue;
					else{
						t.computeRacingPoints(inputPath + i + "/", "log3trail" + j + ".txt");
						Key k = t.k;
						Value v = t.v;
						v.setFileName(i + "/" + t.v.getfileName());
					
						/*if(k.getPos1() == k.getPos2() && k.getPos1()!=0)
						{
							System.out.println("\nWrite-write race: <" + k.getPos1() + ", " + k.getPos2() + "> in file: " + v.getfileName() + " on var " + v.getvarName());
						}*/
					
						if(racingPairs.containsKey(k) == false)
						{
							racingPairs.put(k, v);
						}
					}
						
				}
			}
			System.gc();	// Free up as much memory as possible here
			
			for(Key k : racingPairs.keySet())
			{
				outbr.write("Racing Pair: <" + k.getPos1() + " , " + k.getPos2() + "> in " + racingPairs.get(k).getfileName() + " on var " + racingPairs.get(k).getvarName()+ "\n");
			}
			outbr.close();
		}
			
		long processingTime = System.nanoTime() - startTime;
		System.out.println("\nProcessing time: " + processingTime);
		}

}


