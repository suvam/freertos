package fileGenerator;


	import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
	import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
	import java.io.RandomAccessFile;
	import java.util.*;
	 
	 
	public class testReadReverse {
		
		public Key k;
		public Value v;
	 
		public void computeRacingPoints(String inputPath, String fileName) throws IOException {
			File file = new File(inputPath + fileName);
			// System.out.println("\nProcessing file: " + inputPath + fileName);
			File filew = new File(inputPath + fileName.split("\\.")[0] + "reverse.rev");
			FileWriter write = new FileWriter(filew);
			BufferedWriter buffwrite = new BufferedWriter(write);
			
	 
	        // Reading file in reverse order.. Will return line from 10 to 1
			ReverseLineReaderCore reader = new ReverseLineReaderCore(
					file, "UTF-8");
	        String line;
	        reader.readLine();
	        while ((line = reader.readLine()) != null) {
				//System.out.println(line);
	        	buffwrite.write(line+"\n");
			}
	        reader.close();
	        buffwrite.close();
	        FileReader fileR = new FileReader(filew);
	        BufferedReader buffR = new BufferedReader(fileR);
	        line = "";
	        int pos =1,plus = 0,racepos=0, racepos2=0;
	        String raceVar = "";
	        while ((line = buffR.readLine()) != null) {
	        	//System.out.println("\nChecks inside testReadReverse");
	        	//System.out.println("\nLine: " + line);
	        	//System.out.println("Pos: " + pos);
	        	if((line.contains("_") && (line.contains("= 3") || line.contains("= 4"))) && pos > 75){
	        		// System.out.println("\nInside If, line: " + line);
	        		// System.out.println("\nProcessing line: " + line);
	        		raceVar = line.split("=")[0].trim().split("_")[1];
	        		String line2 = buffR.readLine();
	        		// System.out.println("\nLine 2: " + line2);
	        		racepos2 = Integer.parseInt(line2.split(":")[3].split(" ")[0].trim());
	        		pos++;
	        		plus++;
	        		continue;
	        	}
	        	if(raceVar.length()>0){
	        		if(line.contains(raceVar+"+2")||line.contains(raceVar+"+1")){
	        			plus++;
	        		}
	        		else if(line.contains(raceVar+"-2")||line.contains(raceVar+"-1")){
	        			plus--;
	        		}
	        		if(plus == 2){
	        			racepos = Integer.parseInt(line.split(":")[3].split(" ")[0].trim());
	        			break;
	        		}
	        	}
	        	//System.out.println(pos+" : "+line);
	        	pos++;
	        }
	        // System.out.println(racepos);
	        buffR.close();
	        // System.out.println("\n Pair: " + racepos + " and " + racepos2);
	        k = new Key(racepos, racepos2);
	        v = new Value(fileName, raceVar);
		}
		
		
	}
