/**
 * 
 */
package fileGenerator;

/**
 * @author suvam
 *
 */
public class Key {
	
	private int pos1;
	private int pos2;
	
	public Key()
	{
		pos1=0;
		pos2=0;
	}
	
	public Key(int pos1, int pos2)
	{
		this.pos1 = pos1;
		this.pos2 = pos2;
	}
	
	public void setKey(int pos1, int pos2)
	{
		this.pos1 = pos1;
		this.pos2 = pos2;
	}
	
	public int getPos1()
	{
		return pos1;
	}
	
	public int getPos2()
	{
		return pos2;
	}
	
	@Override
	public boolean equals(Object k1)
	{
		Key k = (Key)k1;
		if((this.pos1 == k.getPos1() && this.pos2 == k.getPos2()) || (this.pos1 == k.getPos2() && this.pos2 == k.getPos1()))
				return true;
		else
			return false;
	}
	
	@Override
	public int hashCode()
	{
		Integer i1 = new Integer(pos1);
		Integer i2 = new Integer(pos2);
		
		int prime1 = 17;
		int prime2 = 29;
		
		return (prime1*i1.hashCode() + prime2*i2.hashCode()) * (prime2*i1.hashCode() + prime1*i2.hashCode());
	}

}

