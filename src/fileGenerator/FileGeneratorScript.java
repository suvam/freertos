package fileGenerator;

/*
 * @Suvam: 
 * 
 * Change Log:
 * 
 * 1. Mar 8: Added support for a third process containing the ISRs
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class FileGeneratorScript {

	public static void main(String[] args) throws IOException {
		
		// Fix to easily allow the files to be generated and executed on different computers
		// Patch by Suvam 
		String relativeAddress = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("\nEnter folder location to store files: ");
		relativeAddress = br.readLine();
		relativeAddress = relativeAddress.trim();
		
		/* @Suvam: Ensure the generated script files have the correct path */
		System.out.println("\nEnter the location of template file: ");
		String template = (br.readLine()).trim();
		System.out.println("\nEnter the location of p1.txt: ");
		String p1 = (br.readLine()).trim();
		System.out.println("\nEnter the location of p2.txt: ");
		String p2 = (br.readLine()).trim();
		System.out.println("\nEnter the location of p3.txt: ");
		String p3 = (br.readLine()).trim();
		
		//String isr = "/home/arun/Desktop/project/withlocks/singlebit/script/isr.txt";
		String line = "";
		String samp = relativeAddress + "files/";
		String scriptpath = relativeAddress + "scripts/";
		String path = relativeAddress;
		String clservpath = "/home/res11/suvam/workspace/freertos/files/";
		ArrayList<String> listp1 = new ArrayList<String>();
		ArrayList<String> listp2 = new ArrayList<String>();
		ArrayList<String> listp3 = new ArrayList<String>();
		try{
			
		    FileReader filep1 = new FileReader(p1);
		    BufferedReader buffp1 = new BufferedReader(filep1);
		    FileReader filep2 = new FileReader(p2);
		    BufferedReader buffp2 = new BufferedReader(filep2);
		    BufferedReader buffp3 = new BufferedReader(new FileReader(p3));
		    
		    //FileWriter fileWriter = new FileWriter(samp);
	       // BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
	        while((line = buffp1.readLine()) != null) {
	        	listp1.add(line);
	        }
	        line = "";
	        while((line = buffp2.readLine()) != null) {
	        	listp2.add(line);
	        }
	        line = "";
	        while((line = buffp3.readLine()) != null) {
	        	listp3.add(line);
	        }
	        line = "";
	        int count = listp1.size()*listp2.size()*listp3.size();
	        int i =1;
	        for(String tp1 : listp1){
        		for(String tp2 : listp2){
        			if(tp1.contains("vQueueDelete") && tp2.contains("vQueueDelete"))
        			{
        				System.out.println("\nvQueueDelete combination: " + i);
        			}
        			for(String tp3 : listp3)
        			{
        				//System.out.println(tp1+"  "+tp2+ "  " +tisr+"\n");
        				String path1 = samp + i + "/"; 
        				String clservpath1 = clservpath + i + "/";
        				if(new File(path1).mkdirs()){
	        				String filename = i+".pml";
	        				String scriptname = i+".sh";
	        				FileReader filetemp = new FileReader(template);
	        			    BufferedReader bufftemp = new BufferedReader(filetemp);
	        				FileWriter fileWriter = new FileWriter(path1+filename);
	        		        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
	        		        FileWriter scriptwriter = new FileWriter(path1+scriptname);
	        		        BufferedWriter bufferedScriptWriter = new BufferedWriter(scriptwriter);
	        		        /*Script to execute the spin */
	        		        bufferedScriptWriter.write("spin -a  "+path1+filename+"; gcc -O2 -DBFS -DXUSAFE -DSAFETY -DNOCLAIM -w -o pan pan.c >log1.txt; ./pan -m100000000000 -c0 -e >log2.txt;");
	        		        /* @Suvam: Deleted text: spin -p -s -r -X -v -n123 -l -g -k "+path1+filename+".trail -u10000 "+path1+filename+" >log3.txt;  */
	        		        /* clserv path*/
	        		        //bufferedScriptWriter.write("spin -a  "+clservpath1+filename+"; gcc -O2 -DBFS -DXUSAFE -DSAFETY -DNOCLAIM -w -o pan pan.c >log1.txt; ./pan -m100000000000 -c0 -e >log2.txt;");
	        		        /*spin code*/
	        				 while((line = bufftemp.readLine()) != null) {
	        			        	
	        			        	if(line.contains("replacep1")){
	        			        		line = tp1;
	        			        	}
	        			        	else if(line.contains("replacep2")){
	        			        		line = tp2;
	        			        	}
	        			        	else if(line.contains("replacep3"))
	        			        	{
	        			        		line = tp3;
	        			        	}
	        			        	
	        			        	//System.out.println(fin + s3.length);
	        			        	bufferedWriter.write(line +"\n");
	        			        }
	        				 bufferedScriptWriter.close();
	        				 bufferedWriter.close();
	        				 bufftemp.close();
	        				 i++;
        			}
        			}
        		}
        	}
	     //  System.out.println(i+ "  "+ count);
	        buffp3.close();
	        buffp2.close();
	        buffp1.close();
	        
		}
		catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file. Please ensure you have template.pml / p1.txt / p2.txt in the folder");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + template + "'");                  
            // Or we could just do this: 
            // ex.printStackTrace();
        }
		
		

	}
}