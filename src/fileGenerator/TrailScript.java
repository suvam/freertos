package fileGenerator;

import java.io.*;

public class TrailScript {

	
	public static void main(String[] args) throws IOException
	{
		long startTime = System.nanoTime();
		String relativePath = "";
		String scriptOutPath = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("\nEnter path to the 'files' folder: ");
		relativePath = br.readLine();
		relativePath = relativePath.trim();
		System.out.println("\nEnter path to output the generated scripts: ");
		scriptOutPath = br.readLine();
		scriptOutPath = scriptOutPath.trim();
		
		
		int startCount;		// starting folder index
		int endCount; 		// ending folder index
		
		System.out.println("\nEnter start folder: ");
		startCount = Integer.parseInt(br.readLine());
		
		System.out.println("\nEnter end folder: ");
		endCount = Integer.parseInt(br.readLine());
		
		for(int i = startCount; i<=endCount; i++)
		{
			File fi = new File(relativePath + i + "/");
			
			int trailFilesCount = fi.listFiles(new FilenameFilter() { 
	   	         public boolean accept(File dir, String filename)
	             { return filename.endsWith(".trail"); }
				} ).length;
			
			String scriptOut = scriptOutPath + i +"/trailGen" + i + ".sh";
			File f2 = new File(scriptOut);
			BufferedWriter br3 = new BufferedWriter(new FileWriter(f2));
			
			for(int j = 1; j <= trailFilesCount; j++)
			{
				br3.write("spin -p -s -r -X -v -n123 -l -g -k "+ relativePath + i + "/" + i + ".pml" + j + ".trail -u100000000000 "+ relativePath + i + "/" + i + ".pml" + " >"+scriptOutPath + i +"/log3trail" + j + ".txt; \n");
			}
			
			br3.close();
			
			
		}
		long processingTime = System.nanoTime() - startTime;
		System.out.println("\nProcessing time: " + processingTime);
	}
	
}
