/**
 * 
 */
package fileGenerator;

/**
 * @author suvam
 *
 */
public class Value {
	

	private String fileName;
	private String varName;
	
	public Value()
	{
		fileName = "";
		varName = "";
	}
	
	public Value(String fileName, String varName)
	{
		this.fileName = fileName;
		this.varName = varName;
	}
	
	public void setValue(String fileName, String varName)
	{
		this.fileName = fileName;
		this.varName = varName;
	}
	
	public String getfileName()
	{
		return fileName;
	}
	
	public String getvarName()
	{
		return varName;
	}
	
	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

}
