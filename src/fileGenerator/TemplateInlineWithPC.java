package fileGenerator;

import java.io.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.*;

/*
 * @author suvam
 */

/*This is a script to replace all the assert inline function call to the function definition */
public class TemplateInlineWithPC {
	
	/*
	 * Populate the libFunctions set with the function names
	 */
	private static void populate(HashSet<String> libFunctions)
	{
		libFunctions.add("vTaskDelay");
		libFunctions.add("vTaskDelayUntil");
		libFunctions.add("vTaskDelete");
		libFunctions.add("xTaskCreate");
		libFunctions.add("xTaskGetTickCount");
		libFunctions.add("uxTaskGetNumberOfTasks");
		libFunctions.add("uxTaskPriorityGet");
		libFunctions.add("vTaskPrioritySet");
		libFunctions.add("vTaskResume");
		libFunctions.add("vTaskSuspend");
		libFunctions.add("xQueueCreate");
		libFunctions.add("vQueueDelete");
		libFunctions.add("vQueueAddToRegistry");
		libFunctions.add("vQueueUnregisterQueue");
		libFunctions.add("uxQueueMessagesWaiting");
		libFunctions.add("xQueueReceive");
		libFunctions.add("xQueueSend");
		libFunctions.add("xTaskGetTickCountFromISR");
		libFunctions.add("xTaskResumeFromISR");
		libFunctions.add("xQueueIsQueueEmptyFromISR");
		libFunctions.add("xQueueIsQueueFullFromISR");
		libFunctions.add("uxQueueMessagesWaitingFromISR");
		libFunctions.add("xQueueReceiveFromISR");
		libFunctions.add("xQueueSendFromISR");
		
		// Auxiliary Functions
		libFunctions.add("prvLockQueue");
		libFunctions.add("prvUnlockQueue");
		libFunctions.add("vTaskIncrementTick");
		libFunctions.add("vTaskMissedYield");
		libFunctions.add("vTaskPlaceOnEventList");
		libFunctions.add("vTaskSuspendAll");
		libFunctions.add("vTaskSwitchContext");
		libFunctions.add("vTaskSwitchContextISR");
		libFunctions.add("xTaskResumeAll");
	}

	public static void main(String[] args) throws IOException {
		String template = "/home/arun/Desktop/project/modelInSpin/Script_files/";
		
		Set<String> libFunctions = new HashSet<String>();
		
		// populate the libFunctions set
		populate((HashSet<String>)libFunctions);
		String funcName = "";
		int index = 0; 
		
		/*String template = "";
		Scanner user = new Scanner(System.in);
		System.out.println("Enter the path of the template file");
		template = user.next();*/
		
		// @Suvam: add support for relative path
		System.out.println("\nPlease enter the full path to load/store template: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String relativePath = "";
		relativePath = br.readLine();
		relativePath = relativePath.trim();
		
		FileReader file = new FileReader(relativePath+"templateIn.pml");
	    BufferedReader buffer = new BufferedReader(file);
	    FileWriter fileWriter = new FileWriter(relativePath+"template.pml");
	    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
	    String line ="";
	    while((line = buffer.readLine())!= null)
	    {
	    	if(line.contains("inline"))	// Could be a function
	    	{
	    		String temp = line;
	    		StringTokenizer st = new StringTokenizer(temp);
	    		st.nextToken();	//This token is just the "inline" keyword
	    		
	    		funcName = st.nextToken();	
	    		funcName = funcName.trim();
	    		funcName = funcName.split("\\(")[0];
	    		//System.out.println("Func Name: " + funcName);
	    	
	    		//check if funcName is indeed a functionName
	    		// If this is a function, reset index to 0
	    		if(libFunctions.contains(funcName))
	    		{
	    			//System.out.println("\nObtained function: " + funcName);
	    			index = 100;
	    		}
	    			
	    	}
	    	
	    	if(line.contains("assert_user")){
	    		if(!line.contains("inline")){
	    			
	    			//bufferedWriter.write(line+" : "+inline(line)+"\n");
	    			
	    			if(libFunctions.contains(funcName))
	    			{
	    				++index;
	    				/*
	    				 * In the first iteration, invoke the function inlineFunc.
	    				 * In subsequent iterations, invoke the function inlineFuncWithConstraints, which adds extra constraints to the assertion
	    				 * to suppress reporting of benign races.
	    				 */
		    			line = line.split("assert_user",2)[0]+inlineFuncWithConstraints(line, funcName, index);	
	    			}
	    			else
	    				line = line.split("assert_user",2)[0]+inline(line);
	    			
	    		}
	    	}
	    	
//	    	if(line.contains("prvLockQueue") && !line.contains("inline"))
//	    	{
//	    		line = "prvLockQueue(pc_"+funcName+","+index+");";
//	    	}
//	    	
//	    	if(line.contains("prvUnlockQueue") && !line.contains("inline"))
//	    	{
//	    		line = "prvUnlockQueue(pc_"+funcName+","+index+");";
//	    	}
	    	bufferedWriter.write(line+"\n");
	    		
	    }
		bufferedWriter.close();
		buffer.close();
	}

	private static String inline(String line)
	{
		String arg = line.split("\\(",2)[1].split("\\)",2)[0];
		String out = "";
		if (line.contains("assert_user_read")){
			out = arg+"="+arg+"+1; assert("+arg+"<3); interrupt(); "+arg+"="+arg+"-1;";
		}
		else if(line.contains("assert_user_write")){
			out = arg+"="+arg+"+2; assert("+arg+"<3);interrupt();"+arg+"="+arg+"-2;";
		}
		else if(line.contains("assert_user_atomic_read")){
			out = arg+"="+arg+"+1; assert("+arg+"<3);"+arg+"="+arg+"-1;";
		}
		else if(line.contains("assert_user_atomic_write")){
			out = arg+"="+arg+"+2; assert("+arg+"<3);"+arg+"="+arg+"-2;";
		}
		else
		{
			System.out.println("Found a case where assert is not in any if cases in inline \n");
			System.exit(0);
		}
		return out;
		
	}
	
	private static String inlineFunc(String line, String funcName, int index) {
		String arg = line.split("\\(",2)[1].split("\\)",2)[0];
		String out = "";
		if (line.contains("assert_user_read")){
			out = "pc_" + funcName + "=" + index + "; " + arg+"="+arg+"+1; assert("+arg+"<3);" +
					"interrupt(); "+arg+"="+arg+"-1;";
		}
		else if(line.contains("assert_user_write")){
			out = "pc_" + funcName + "=" + index + "; " + arg+"="+arg+"+2; assert("+arg+"<3);interrupt();"+arg+"="+arg+"-2;";
		}
		else if(line.contains("assert_user_atomic_read")){
			out = "pc_" + funcName + "=" + index + "; " + arg+"="+arg+"+1; assert("+arg+"<3);"+arg+"="+arg+"-1;";
		}
		else if(line.contains("assert_user_atomic_write")){
			out = "pc_" + funcName + "=" + index + ";" + arg+"="+arg+"+2; assert("+arg+"<3);"+arg+"="+arg+"-2;";
		}
		else
		{
			System.out.println("Found a case where assert is not in any if cases in inline \n");
			System.exit(0);
		}
		return out;
	}
	
	/*
	 * main should call this function after the first iteration, when assertion violations are discovered. This method augments the original 
	 * assertion with additional constraints, such that benign races in iteration i are not reported in iteration i+1. The constraints for the 
	 * races on the queue have been added for demonstration purposes, but are commented out because the race on the queue is fixed in the model,
	 * and wouldn't show up in subsequent iterations anyway.
	 */
	
	private static String inlineFuncWithConstraints(String line, String funcName, int index)
	{
		String arg = line.split("\\(",2)[1].split("\\)",2)[0];
		String out = "";
		if (line.contains("assert_user_read")){
			out = "pc_" + funcName + "=" + index + "; " + arg+"="+arg+"+1; assert("+arg+"<3 ||" +
					// Constraints generated after Iteration 1
					// Some of the constraints below have been commented out because of Spin's limitation of the size of the inline, and because of fixes made to the code
					//"(pc_vQueueAddToRegistry == 101 && pc_vQueueUnregisterQueue == 103 && pc_xTaskGetTickCountFromISR == 101) ||" + 
					//"(pc_vQueueAddToRegistry == 105 && pc_xTaskGetTickCountFromISR == 101) ||" + 
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaitingFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueFullFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueReceive == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"( pc_vQueueDelete == 101 &&  pc_vQueueUnregisterQueue == 101 pc_xQueueReceiveFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 101 && pc_uxTaskGetNumberOfTasks == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueReceiveFromISR == 103) ||" +
					//"(pc_vQueueAddToRegistry == 105 && pc_vQueueUnregisterQueue == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 103) ||" +
					//"(pc_vQueueUnregisterQueue == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueEmptyFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaiting == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueSendFromISR == 103) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueReceiveFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					"(pc_vTaskResume == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueSend == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 119 && pc_vTaskPrioritySet == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueSend == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueReceiveFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueReceiveFromISR == 103) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueEmptyFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueSend == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueReceive == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 103 && pc_vTaskResume == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					"(pc_xTaskCreate == 120 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) );" +	// NOTE! Actual end of constraints here
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaitingFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueEmptyFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueFullFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueReceive == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaitingFromISR == 101 &&  pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueFullFromISR == 101) );" + 
					
					"interrupt(); "+arg+"="+arg+"-1;";
		}
		
		else if(line.contains("assert_user_write")){
			out = "pc_" + funcName + "=" + index + "; " + arg+"="+arg+"+2; assert("+arg+"<3 ||" + 
					// Constraints generated after Iteration 1
					// Some of the constraints below have been commented out because of Spin's limitation of the size of the inline, and because of fixes made to the code
					//"(pc_vQueueAddToRegistry == 101 && pc_vQueueUnregisterQueue == 103 && pc_xTaskGetTickCountFromISR == 101) ||" + 
					//"(pc_vQueueAddToRegistry == 105 && pc_xTaskGetTickCountFromISR == 101) ||" + 
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaitingFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueFullFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueReceive == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"( pc_vQueueDelete == 101 &&  pc_vQueueUnregisterQueue == 101 pc_xQueueReceiveFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 101 && pc_uxTaskGetNumberOfTasks == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueReceiveFromISR == 103) ||" +
					//"(pc_vQueueAddToRegistry == 105 && pc_vQueueUnregisterQueue == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 103) ||" +
					//"(pc_vQueueUnregisterQueue == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueEmptyFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaiting == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueSendFromISR == 103) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueReceiveFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					"(pc_vTaskResume == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueSend == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 119 && pc_vTaskPrioritySet == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueSend == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueReceiveFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueReceiveFromISR == 103) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueEmptyFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueSend == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueReceive == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 103 && pc_vTaskResume == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					"(pc_xTaskCreate == 120 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) );" +	// NOTE! Actual end of constraints here
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaitingFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueEmptyFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueFullFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueReceive == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaitingFromISR == 101 &&  pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueFullFromISR == 101) );" + 
					
					"interrupt(); "+arg+"="+arg+"-2;";
		}
		
		else if(line.contains("assert_user_atomic_read")){
			out = "pc_" + funcName + "=" + index + "; " + arg+"="+arg+"+1; assert("+arg+"<3 ||" +
					// Constraints generated after Iteration 1
					// Some of the constraints below have been commented out because of Spin's limitation of the size of the inline, and because of fixes made to the code
					//"(pc_vQueueAddToRegistry == 101 && pc_vQueueUnregisterQueue == 103 && pc_xTaskGetTickCountFromISR == 101) ||" + 
					//"(pc_vQueueAddToRegistry == 105 && pc_xTaskGetTickCountFromISR == 101) ||" + 
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaitingFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueFullFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueReceive == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"( pc_vQueueDelete == 101 &&  pc_vQueueUnregisterQueue == 101 pc_xQueueReceiveFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 101 && pc_uxTaskGetNumberOfTasks == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueReceiveFromISR == 103) ||" +
					//"(pc_vQueueAddToRegistry == 105 && pc_vQueueUnregisterQueue == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 103) ||" +
					//"(pc_vQueueUnregisterQueue == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueEmptyFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaiting == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueSendFromISR == 103) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueReceiveFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					"(pc_vTaskResume == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueSend == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 119 && pc_vTaskPrioritySet == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueSend == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueReceiveFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueReceiveFromISR == 103) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueEmptyFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueSend == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueReceive == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 103 && pc_vTaskResume == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					"(pc_xTaskCreate == 120 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) );" +	// NOTE! Actual end of constraints here
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaitingFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueEmptyFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueFullFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueReceive == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaitingFromISR == 101 &&  pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueFullFromISR == 101) );" + 
					
					arg+"="+arg+"-1;";		
		}
		
		else if(line.contains("assert_user_atomic_write")){
			out = "pc_" + funcName + "=" + index + ";" + arg+"="+arg+"+2; assert("+arg+"<3 ||"+
					// Constraints generated after Iteration 1
					// Some of the constraints below have been commented out because of Spin's limitation of the size of the inline, and because of fixes made to the code
					//"(pc_vQueueAddToRegistry == 101 && pc_vQueueUnregisterQueue == 103 && pc_xTaskGetTickCountFromISR == 101) ||" + 
					//"(pc_vQueueAddToRegistry == 105 && pc_xTaskGetTickCountFromISR == 101) ||" + 
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaitingFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueFullFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueReceive == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"( pc_vQueueDelete == 101 &&  pc_vQueueUnregisterQueue == 101 pc_xQueueReceiveFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 101 && pc_uxTaskGetNumberOfTasks == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueReceiveFromISR == 103) ||" +
					//"(pc_vQueueAddToRegistry == 105 && pc_vQueueUnregisterQueue == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 103) ||" +
					//"(pc_vQueueUnregisterQueue == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueEmptyFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaiting == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueSendFromISR == 103) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueReceiveFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					"(pc_vTaskResume == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueSend == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 119 && pc_vTaskPrioritySet == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueSend == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueReceiveFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueReceiveFromISR == 103) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueEmptyFromISR == 101) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueSend == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_xQueueReceive == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					"(pc_xTaskCreate == 103 && pc_vTaskResume == 101 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueSendFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					"(pc_xTaskCreate == 120 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) );" +	// NOTE! Actual end of constraints here
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaitingFromISR == 101) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueEmptyFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueFullFromISR == 101 && pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueAddToRegistry == 103 && pc_xQueueReceive == 103 && pc_xTaskGetTickCountFromISR == 101) ||" +
					//"(pc_vQueueUnregisterQueue == 101 && pc_uxQueueMessagesWaitingFromISR == 101 &&  pc_vTaskIncrementTick == 122 && pc_vTaskSwitchContextISR == 104) ||" +
					//"(pc_vQueueDelete == 101 && pc_vQueueUnregisterQueue == 101 && pc_xQueueIsQueueFullFromISR == 101) );" + 
					
					arg+"="+arg+"-2;";
		}
			
		else
		{
			System.out.println("Found a case where assert is not in any if cases in inline \n");
			System.exit(0);
		}
		return out;
		
	}

	private static String inlineFuncWithConstraints2(String line, String funcName, int index) {
		String arg = line.split("\\(",2)[1].split("\\)",2)[0];
		String out = "";
		if (line.contains("assert_user_read")){
			out = "pc_" + funcName + "=" + index + "; " + arg+"="+arg+"+1; assert("+arg+"<3 ||" +
					//"(pc_vQueueUnregisterQueue == 103 && pc_vQueueAddToRegistry == 101) ||" +
					//"(pc_vQueueAddToRegistry == 105 && pc_vQueueAddToRegistry == 101) || " +
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueUnregisterQueue==101 && pc_vTaskIncrementTick==122 && pc_vTaskSwitchContextISR==104) ||" +
					//"(pc_uxQueueMessagesWaitingFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceive==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueReceiveFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_uxQueueMessagesWaitingFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_vQueueDelete==101 && pc_vQueueAddToRegistry==103) ||" +
					//"(pc_vQueueDelete==101 && pc_vQueueUnregisterQueue==101) ||" +
					"(pc_vTaskSwitchContextISR==104 && pc_vTaskResume==101) ||" +
					"(pc_xTaskCreate==101 && pc_uxTaskGetNumberOfTasks==101) ||" +
					//"(pc_xQueueReceiveFromISR==103 && pc_vQueueAddToRegistry==103) ||" +
					"(pc_xTaskCreate==103 && pc_xTaskCreate==120) ||" +
					//"(pc_vQueueUnregisterQueue==103 && pc_vQueueAddToRegistry==105) ||" +
					"(pc_vTaskSwitchContextISR==104 && pc_xTaskCreate==120) ||" + 
					//"(pc_uxQueueMessagesWaitingFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueDelete==101) ||" + 
					//"(pc_vQueueUnregisterQueue==103 && pc_vQueueUnregisterQueue==103) ||" +
					//"(pc_xQueueIsQueueEmptyFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueSendFromISR==103 && pc_vQueueAddToRegistry==103) ||" +
					//"(pc_xQueueSendFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceiveFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					"(pc_vTaskPrioritySet==103 && pc_xTaskCreate==119) ||" +
					//"(pc_xQueueSend==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueSendFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueReceiveFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceiveFromISR==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueIsQueueEmptyFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueSend==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueReceive==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueSendFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueIsQueueEmptyFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_uxQueueMessagesWaiting==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceive==103 && pc_vQueueAddToRegistry==103) ||" +
					"(pc_xTaskCreate==103 && pc_vTaskResume==101) ||" +
					
					// Run 3 constraints
					"(pc_vTaskDelay==104 && pc_vTaskResume==101) || " +
					"(pc_vTaskDelayUntil==104 && pc_vTaskResume==101) ||" +
					"(pc_vTaskResume==101 && pc_xQueueReceiveFromISR==106) ||" +
					"(pc_vTaskResume==106 && pc_xTaskGetTickCountFromISR==101) ||" +
					"(pc_vTaskDelayUntil==106 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelayUntil==102 && pc_vTaskResume==101) || " +
					"(pc_vTaskDelay==102 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelayUntil==108 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelay==106 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelay==108 && pc_vTaskResume==101));" +
					
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueDelete==101));" +
					"interrupt(); "+arg+"="+arg+"-1;";
		}
		else if(line.contains("assert_user_write")){
			out = "pc_" + funcName + "=" + index + "; " + arg+"="+arg+"+2; assert("+arg+"<3 ||" + 
					//"(pc_vQueueUnregisterQueue == 103 && pc_vQueueAddToRegistry == 101) ||" +
					//"(pc_vQueueAddToRegistry == 105 && pc_vQueueAddToRegistry == 101) || " +
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueUnregisterQueue==101 && pc_vTaskIncrementTick==122 && pc_vTaskSwitchContextISR==104) ||" +
					//"(pc_uxQueueMessagesWaitingFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceive==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueReceiveFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_uxQueueMessagesWaitingFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_vQueueDelete==101 && pc_vQueueAddToRegistry==103) ||" +
					//"(pc_vQueueDelete==101 && pc_vQueueUnregisterQueue==101) ||" +
					"(pc_vTaskSwitchContextISR==104 && pc_vTaskResume==101) ||" +
					"(pc_xTaskCreate==101 && pc_uxTaskGetNumberOfTasks==101) ||" +
					//"(pc_xQueueReceiveFromISR==103 && pc_vQueueAddToRegistry==103) ||" +
					"(pc_xTaskCreate==103 && pc_xTaskCreate==120) ||" +
					//"(pc_vQueueUnregisterQueue==103 && pc_vQueueAddToRegistry==105) ||" +
					"(pc_vTaskSwitchContextISR==104 && pc_xTaskCreate==120) ||" + 
					//"(pc_uxQueueMessagesWaitingFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueDelete==101) ||" + 
					//"(pc_vQueueUnregisterQueue==103 && pc_vQueueUnregisterQueue==103) ||" +
					//"(pc_xQueueIsQueueEmptyFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueSendFromISR==103 && pc_vQueueAddToRegistry==103) ||" +
					//"(pc_xQueueSendFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceiveFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					"(pc_vTaskPrioritySet==103 && pc_xTaskCreate==119) ||" +
					//"(pc_xQueueSend==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueSendFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueReceiveFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceiveFromISR==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueIsQueueEmptyFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueSend==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueReceive==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueSendFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueIsQueueEmptyFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_uxQueueMessagesWaiting==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceive==103 && pc_vQueueAddToRegistry==103) ||" +
					"(pc_xTaskCreate==103 && pc_vTaskResume==101) ||" +
					
					// Run 3 constraints
					"(pc_vTaskDelay==104 && pc_vTaskResume==101) || " +
					"(pc_vTaskDelayUntil==104 && pc_vTaskResume==101) ||" +
					"(pc_vTaskResume==101 && pc_xQueueReceiveFromISR==106) ||" +
					"(pc_vTaskResume==106 && pc_xTaskGetTickCountFromISR==101) ||" +
					"(pc_vTaskDelayUntil==106 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelayUntil==102 && pc_vTaskResume==101) || " +
					"(pc_vTaskDelay==102 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelayUntil==108 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelay==106 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelay==108 && pc_vTaskResume==101));" +
	
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueDelete==101));" +
					"interrupt();"+arg+"="+arg+"-2;";
		}
		else if(line.contains("assert_user_atomic_read")){
			out = "pc_" + funcName + "=" + index + "; " + arg+"="+arg+"+1; assert("+arg+"<3 ||" +
					//"(pc_vQueueUnregisterQueue == 103 && pc_vQueueAddToRegistry == 101) ||" +
					//"(pc_vQueueAddToRegistry == 105 && pc_vQueueAddToRegistry == 101) || " +
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueUnregisterQueue==101 && pc_vTaskIncrementTick==122 && pc_vTaskSwitchContextISR==104) ||" +
					//"(pc_uxQueueMessagesWaitingFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceive==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueReceiveFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_uxQueueMessagesWaitingFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_vQueueDelete==101 && pc_vQueueAddToRegistry==103) ||" +
					//"(pc_vQueueDelete==101 && pc_vQueueUnregisterQueue==101) ||" +
					"(pc_vTaskSwitchContextISR==104 && pc_vTaskResume==101) ||" +
					"(pc_xTaskCreate==101 && pc_uxTaskGetNumberOfTasks==101) ||" +
					//"(pc_xQueueReceiveFromISR==103 && pc_vQueueAddToRegistry==103) ||" +
					"(pc_xTaskCreate==103 && pc_xTaskCreate==120) ||" +
					//"(pc_vQueueUnregisterQueue==103 && pc_vQueueAddToRegistry==105) ||" +
					"(pc_vTaskSwitchContextISR==104 && pc_xTaskCreate==120) ||" + 
					//"(pc_uxQueueMessagesWaitingFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueDelete==101) ||" + 
					//"(pc_vQueueUnregisterQueue==103 && pc_vQueueUnregisterQueue==103) ||" +
					//"(pc_xQueueIsQueueEmptyFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueSendFromISR==103 && pc_vQueueAddToRegistry==103) ||" +
					//"(pc_xQueueSendFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceiveFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					"(pc_vTaskPrioritySet==103 && pc_xTaskCreate==119) ||" +
					//"(pc_xQueueSend==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueSendFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueReceiveFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceiveFromISR==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueIsQueueEmptyFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueSend==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueReceive==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueSendFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueIsQueueEmptyFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_uxQueueMessagesWaiting==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceive==103 && pc_vQueueAddToRegistry==103) ||" +
					"(pc_xTaskCreate==103 && pc_vTaskResume==101) ||" +
					
					// Run 3 constraints
					"(pc_vTaskDelay==104 && pc_vTaskResume==101) || " +
					"(pc_vTaskDelayUntil==104 && pc_vTaskResume==101) ||" +
					"(pc_vTaskResume==101 && pc_xQueueReceiveFromISR==106) ||" +
					"(pc_vTaskResume==106 && pc_xTaskGetTickCountFromISR==101) ||" +
					"(pc_vTaskDelayUntil==106 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelayUntil==102 && pc_vTaskResume==101) || " +
					"(pc_vTaskDelay==102 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelayUntil==108 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelay==106 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelay==108 && pc_vTaskResume==101));" +
															
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueDelete==101));" +
					arg+"="+arg+"-1;";
		}
		else if(line.contains("assert_user_atomic_write")){
			out = "pc_" + funcName + "=" + index + ";" + arg+"="+arg+"+2; assert("+arg+"<3 ||"+
					//"(pc_vQueueUnregisterQueue == 103 && pc_vQueueAddToRegistry == 101) ||" +
					//"(pc_vQueueAddToRegistry == 105 && pc_vQueueAddToRegistry == 101) || " +
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueUnregisterQueue==101 && pc_vTaskIncrementTick==122 && pc_vTaskSwitchContextISR==104) ||" +
					//"(pc_uxQueueMessagesWaitingFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceive==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueReceiveFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_uxQueueMessagesWaitingFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_vQueueDelete==101 && pc_vQueueAddToRegistry==103) ||" +
					//"(pc_vQueueDelete==101 && pc_vQueueUnregisterQueue==101) ||" +
					"(pc_vTaskSwitchContextISR==104 && pc_vTaskResume==101) ||" +
					"(pc_xTaskCreate==101 && pc_uxTaskGetNumberOfTasks==101) ||" +
					//"(pc_xQueueReceiveFromISR==103 && pc_vQueueAddToRegistry==103) ||" +
					"(pc_xTaskCreate==103 && pc_xTaskCreate==120) ||" +
					//"(pc_vQueueUnregisterQueue==103 && pc_vQueueAddToRegistry==105) ||" +
					"(pc_vTaskSwitchContextISR==104 && pc_xTaskCreate==120) ||" + 
					//"(pc_uxQueueMessagesWaitingFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueDelete==101) ||" + 
					//"(pc_vQueueUnregisterQueue==103 && pc_vQueueUnregisterQueue==103) ||" +
					//"(pc_xQueueIsQueueEmptyFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueSendFromISR==103 && pc_vQueueAddToRegistry==103) ||" +
					//"(pc_xQueueSendFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceiveFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					"(pc_vTaskPrioritySet==103 && pc_xTaskCreate==119) ||" +
					//"(pc_xQueueSend==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueSendFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueReceiveFromISR==101 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceiveFromISR==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueIsQueueEmptyFromISR==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueSend==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueReceive==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueSendFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueIsQueueEmptyFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_uxQueueMessagesWaiting==101 && pc_vQueueDelete==101) ||" +
					//"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueAddToRegistry==103 && pc_vQueueUnregisterQueue==101) ||" +
					//"(pc_xQueueReceive==103 && pc_vQueueAddToRegistry==103) ||" +
					"(pc_xTaskCreate==103 && pc_vTaskResume==101) ||" +
					
					// Run 3 constraints
					"(pc_vTaskDelay==104 && pc_vTaskResume==101) || " +
					"(pc_vTaskDelayUntil==104 && pc_vTaskResume==101) ||" +
					"(pc_vTaskResume==101 && pc_xQueueReceiveFromISR==106) ||" +
					"(pc_vTaskResume==106 && pc_xTaskGetTickCountFromISR==101) ||" +
					"(pc_vTaskDelayUntil==106 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelayUntil==102 && pc_vTaskResume==101) || " +
					"(pc_vTaskDelay==102 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelayUntil==108 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelay==106 && pc_vTaskResume==101) ||" +
					"(pc_vTaskDelay==108 && pc_vTaskResume==101));" +
										
					//	"(pc_xQueueIsQueueFullFromISR==101 && pc_vQueueDelete==101));" +
					arg+"="+arg+"-2;";
		}
		else
		{
			System.out.println("Found a case where assert is not in any if cases in inline \n");
			System.exit(0);
		}
		return out;
	}

}
