/**
 * 
 */
package fileGenerator;

/**
 * @author suvam
 *
 * Class to read up the log files and build the set of racing pairs.
 * The hash table is a <key, value> tuple. The key is a tuple containing the racing line numbers. The value contains
 * the filename and the variable involved in the race. 
 *
 */

import java.util.*;
import java.io.*;

public class BuildRacingPairs {

	public static void main(String[] args) throws IOException
	{
		Hashtable<Key, Value> racingPairs = new Hashtable<Key, Value>();
		String inputPath = "";
		String outPath = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("\nPlease enter input file path: ");
		inputPath = br.readLine();
		inputPath = inputPath.trim();
		
		System.out.println("\nPlease enter output file path: ");
		outPath = br.readLine();
		outPath = outPath.trim();
		File outf = new File(outPath);
		BufferedWriter outbr = new BufferedWriter(new FileWriter(outf));
		
		int start =1,end = 1;
		System.out.println("\nPlease enter Starting file: ");
		start = Integer.parseInt(br.readLine().trim());
		System.out.println("\nPlease enter ending file: ");
		end = Integer.parseInt(br.readLine().trim());
		for(int i=start; i <= end; i++)
		{
			System.out.println("\nAnalyzing directory: " + i);
			// System.out.println(inputPath + i + "/");
			File f = new File(inputPath + i + "/");
			
			int trailFilesCount = f.listFiles(new FilenameFilter() { 
	   	         public boolean accept(File dir, String filename)
	             { return filename.endsWith(".txt"); }
				} ).length;
		
			for(int j = 1;j<=trailFilesCount;j++){
				testReadReverse t = new testReadReverse();
				t.computeRacingPoints(inputPath + i + "/", "log3trail" + j + ".txt");
				Key k = t.k;
				Value v = t.v;
				v.setFileName(i + "/" + t.v.getfileName());
				
				if(racingPairs.containsKey(k) == false)
				{
					racingPairs.put(k, v);
				}
					
			}
		}
		System.gc();	// Free up as much memory as possible here
		
		for(Key k : racingPairs.keySet())
		{
			outbr.write("Racing Pair: (" + k.getPos1() + " ), (" + k.getPos2() + ") in " + racingPairs.get(k).getfileName() + " on var " + racingPairs.get(k).getvarName()+ "\n");
		}
		outbr.close();
	}
}
