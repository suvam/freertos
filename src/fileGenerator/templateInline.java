package fileGenerator;

import java.io.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/*This is a script to replace all the assert inline function call to the function definition */
public class templateInline {

	public static void main(String[] args) throws IOException {
		String template = "/home/arun/Desktop/project/modelInSpin/Script_files/";
		/*String template = "";
		Scanner user = new Scanner(System.in);
		System.out.println("Enter the path of the template file");
		template = user.next();*/
		
		// @Suvam: add support for relative path
		System.out.println("\nPlease enter the full path to load/store template: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String relativePath = "";
		relativePath = br.readLine();
		relativePath = relativePath.trim();
		
		FileReader file = new FileReader(relativePath+"templateIn.pml");
	    BufferedReader buffer = new BufferedReader(file);
	    FileWriter fileWriter = new FileWriter(relativePath+"template.pml");
	    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
	    String line ="";
	    while((line = buffer.readLine())!= null){
	    	if(line.contains("assert_user")){
	    		if(!line.contains("inline")){
	    			
	    			//bufferedWriter.write(line+" : "+inline(line)+"\n");
	    			line = line.split("assert_user",2)[0]+inline(line);
	    		}
	    	}
	    	bufferedWriter.write(line+"\n");
	    		
	    }
		bufferedWriter.close();
		buffer.close();
	}

	private static String inline(String line) {
		String arg = line.split("\\(",2)[1].split("\\)",2)[0];
		String out = "";
		if (line.contains("assert_user_read")){
			out = arg+"="+arg+"+1;assert("+arg+"<3);interrupt();"+arg+"="+arg+"-1;";
		}
		else if(line.contains("assert_user_write")){
			out = arg+"="+arg+"+2;assert("+arg+"<3);interrupt();"+arg+"="+arg+"-2;";
		}
		else if(line.contains("assert_user_atomic_read")){
			out = arg+"="+arg+"+1;assert("+arg+"<3);"+arg+"="+arg+"-1;";
		}
		else if(line.contains("assert_user_atomic_write")){
			out = arg+"="+arg+"+2;assert("+arg+"<3);"+arg+"="+arg+"-2;";
		}
		else
		{
			System.out.println("Found a case where assert is not in any if cases in inline \n");
			System.exit(0);
		}
		return out;
	}

}
