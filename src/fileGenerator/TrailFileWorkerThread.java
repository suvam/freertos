/**
 * Used to parallelize the processing of the trail files. 
 */
package fileGenerator;

/**
 * @author suvam
 * 2016. Macbook Pro Retina.
 *
 */
import java.io.*;

public class TrailFileWorkerThread extends Thread {
	
	private String directory;
	private String subDirectory;
	private BufferedWriter logOut;
	
	public TrailFileWorkerThread(String directory, String subDirectory, BufferedWriter logOut)
	{
		this.directory = directory;
		this.subDirectory = subDirectory;
		this.logOut = logOut;
	}
	
	public void run()
	{
		try
		{
			logOut.write("\nNow processing directory: " + subDirectory);
		}
		catch(IOException e)
		{
			System.out.println("\nAn I/O Exception occurred while writing to log file in " + subDirectory);
		}
		String command = "cd " + directory + subDirectory + " && ./trailGen" + subDirectory + ".sh"; 
		System.out.println("\nExecuting command: " + command);
		Process p;
		try
		{
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
		}
		catch(IOException e)
		{
			System.out.println("\nAn I/O Error occurred while processing directory " + directory + subDirectory);
		}
		catch(InterruptedException e)
		{
			System.out.println("\nAn Interrupted Exception occurred while processing directory " + directory + subDirectory);
		}
		
	}

}
