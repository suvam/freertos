# Suvam Mukherjee
echo 'Starting verification run' >> /Volumes/TRANSCEND/workspace/exp/vmcai17/it2/runLog.txt
start=`date +%s` 
for i in {1..2023} 
do 
 dirStart=`date +%s`
 echo 'Now processing directory: ' $i >> /Volumes/TRANSCEND/workspace/exp/vmcai17/it2/runLog.txt
 cd /Volumes/TRANSCEND/workspace/exp/vmcai17/it2/files/$i && ./$i.sh
 dirStop=`date +%s`
 dirRuntime=$((dirStop-dirStart))
 echo 'Running time: ' $dirRuntime >> /Volumes/TRANSCEND/workspace/exp/vmcai17/it2/runLog.txt
done 
end=`date +%s` 
runtime=$((end-start)) 
echo 'Net running time: ' $runtime >> /Volumes/TRANSCEND/workspace/exp/vmcai17/it2/runLog.txt

