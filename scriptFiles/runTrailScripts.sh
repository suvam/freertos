# Suvam Mukherjee
echo 'Starting processing trail files' >> /media/suvam/dataDrive/workspace/exp/allRaces/trailLog.txt
start=`date +%s`
for i in {1..2023}
do
 echo 'Now processing directory: ' $i >> /media/suvam/dataDrive/workspace/exp/allRaces/trailLog.txt
 cd /media/suvam/dataDrive/workspace/exp/allRaces/files/$i
 ./trailGen$i.sh &
done
wait
end=`date +%s`
runtime=$((end-start))
echo 'Net running time: ' $runtime
echo 'Net running time: ' $runtime >> /media/suvam/dataDrive/workspace/exp/allRaces/trailLog.txt

