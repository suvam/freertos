// FreeRTOS API model
// Suvam Mukherjee

// This version contains the model, with fixes

chan sh_lc = [1] of { bit};
chan isr_lc = [1] of { bit};
chan api_lc = [1] of { bit};
chan sus_sh_lc = [1] of { bit};

bit c;
bit schedulerRunning;
byte xRx = 0, xTx = 0;//queue lock from ISR
byte _xQueueRegistry;
int uxCurrentNumberOfTasks=0,SchedulerSuspended = 0,xMissedYield=0;

// Add a program counter variable for each library function
int pc_vTaskDelay = 0;
int pc_vTaskDelayUntil = 0;
int pc_vTaskDelete = 0;
int pc_xTaskCreate = 0;
int pc_xTaskGetTickCount = 0;
int pc_uxTaskGetNumberOfTasks = 0;
int pc_uxTaskPriorityGet = 0;
int pc_vTaskPrioritySet = 0;
int pc_vTaskResume = 0;
int pc_vTaskSuspend = 0;
int pc_xQueueCreate = 0;
int pc_vQueueDelete = 0;
int pc_vQueueAddToRegistry = 0;
int pc_vQueueUnregisterQueue = 0;
int pc_uxQueueMessagesWaiting = 0;
int pc_xQueueReceive = 0;
int pc_xQueueSend = 0;

// fromISR function Program Counters
int pc_xTaskGetTickCountFromISR = 0;
int pc_xTaskResumeFromISR = 0;
int pc_xQueueIsQueueEmptyFromISR = 0;
int pc_xQueueIsQueueFullFromISR = 0;
int pc_uxQueueMessagesWaitingFromISR = 0;
int pc_xQueueReceiveFromISR = 0;
int pc_xQueueSendFromISR = 0;

// Auxiliary Program variables
int pc_prvLockQueue = 0;
int pc_prvUnlockQueue = 0;
int pc_vTaskIncrementTick = 0;
int pc_vTaskMissedYield = 0;
int pc_vTaskPlaceOnEventList = 0;
int pc_vTaskSuspendAll = 0;
int pc_vTaskSwitchContext = 0;
int pc_vTaskSwitchContextISR = 0;
int pc_xTaskResumeAll = 0;

byte _uxTopReadyPriority = 0;
byte _userQueue = 0;
//byte _uxSchedulerSuspended = 0;
byte _uxPriority = 0; // priority of a particular task ( can be accessed outside of the task)
byte _xTickCount =0,_xNumOfOverflows=0,_uxMissedTicks=0,_pxCurrentTCB=0,_uxTaskNumber=0, _uxTopUsedPriority = 0,_uxCurrentNumberOfTasks = 0,_uxTasksDeleted = 0;
byte _pxReadyTasksLists =0, _xTasksWaitingTermination =0,  _pxOverflowDelayedTaskList =0, _pxDelayedTaskList =0, _xSuspendedTaskList =0, _xPendingReadyList =0,_xTasksWaitingToSend=0,_xTasksWaitingToReceive=0;   // Queues used by RTOS
byte _uxMessagesWaiting = 0, _uxDataInQueue = 0;

int count = 0;
inline getLock(l){

	l ? c;	
	
}
inline releaseLock(l){
	l ! c;
}

inline interrupt(){
	if   // interrupt 
	:: skip -> 
		if 
		:: SchedulerSuspended == 1 -> releaseLock(isr_lc);getLock(sus_sh_lc);
		:: else -> releaseLock(isr_lc);getLock(api_lc);
		fi
	:: skip -> skip;	// skip interrupt
	fi  // return from interrupt
}

inline choose(){
		/* This part is modified in this version */
	/*if 
	:: schedulerRunning == 0 -> releaseLock(sus_sh_lc);
	:: else -> releaseLock(sh_lc);
	fi*/
	releaseLock(sh_lc);
}

proctype scheduler(){
/* This part is modified in this version */
	do 
		//:: skip ->releaseLock(api_lc); getLock(sh_lc);    // reschedules and waits
		:: SchedulerSuspended == 1 -> 
				if
					:: skip-> releaseLock(isr_lc); getLock(sh_lc);
					:: skip-> releaseLock(sus_sh_lc); getLock(sh_lc);
				fi
		
		:: else ->
				if
					:: skip -> releaseLock(isr_lc); getLock(sh_lc);
					:: skip -> releaseLock(api_lc); getLock(sh_lc);
				fi
		
	od
}

inline portYIELD_WITHIN_API() // yield p1 or p2
{
	releaseLock(sh_lc);
	getLock(api_lc)

}

inline prvLockQueue()
{
	interrupt();
	// read inside if statements to check for values of xRxLock and xTxLock
	assert_user_atomic_read(_userQueue);
	if 
	:: xRx == 0 -> xRx = 1;
	:: else -> skip
	fi
	if 
	:: xTx == 0 -> xTx = 1;
	:: else -> skip
	fi
}

inline prvUnlockQueue(  )
{
	interrupt();

	//atomic
	//{
		// read inside while check statements to check for the value of xRxLock 
			assert_user_atomic_read(_userQueue);
		do 
		:: xTx > 1 ->
			//Data was posted while the queue was locked. 
			assert_user_atomic_read(_xTasksWaitingToReceive);
			if
			// waiting to receive is not empty
			:: skip -> 
				xTaskRemoveFromEventList( _xTasksWaitingToReceive);
				if 
				:: skip ->
						vTaskMissedYield();
				:: skip-> skip;
				fi
/*** change 5 ***/
				assert_user_atomic_read(_userQueue);
/*** end 5 ***/
				xTx = xTx -1;
			:: skip -> 
				break;
			fi
		:: else -> break;
		od
/*** change 5 ***/
		assert_user_atomic_read(_userQueue);
/*** end 5 ***/
		xTx = 0;
	//} // end of atomic

	interrupt();

	//atomic
	//{
		// read inside while check statements to check for the value of xTxLock
		assert_user_atomic_read(_userQueue);
		do 
		:: xRx > 1 ->
			//Data was posted while the queue was locked. 
			assert_user_atomic_read(_xTasksWaitingToSend);
			if
			// waiting to send is not empty
			:: skip -> 
				xTaskRemoveFromEventList( _xTasksWaitingToSend);
				if 
				:: skip ->
						vTaskMissedYield();
				:: skip-> skip;
				fi
/*** change 5 ***/
				assert_user_atomic_read(_userQueue);
/*** end 5 ***/
				xRx = xRx -1;
			:: skip -> 
				break;
			fi
		:: else -> break;
		od
/*** change 5 ***/
		assert_user_atomic_read(_userQueue);
/*** end 5 ***/
		xRx = 0;
	//} // end of atomic
}

inline assert_user_read(b){
	b=b+1;
	assert(b<3);
	interrupt();
	b=b-1;
}
inline assert_user_write(b){
	b=b+2;
	assert(b<3);
	interrupt();
	b=b-2;

}
inline assert_user_atomic_read(b){
	b=b+1;
	assert(b<3);
	b=b-1;
}
inline assert_user_atomic_write(b){
	b=b+2;
	assert(b<3);
	b=b-2;
}

inline uxQueueMessagesWaiting()
{
	interrupt();
//	atomic{
// Read from a user queue 
		assert_user_atomic_read(_userQueue);
		assert_user_atomic_read(_uxMessagesWaiting);
	//}
}

inline uxQueueMessagesWaitingFromISR()
{
		// Read from a user queue 
		assert_user_atomic_read(_userQueue);
		assert_user_atomic_read(_uxMessagesWaiting);
}

inline uxTaskGetNumberOfTasks()
{
	interrupt();

	/* A critical section is not required because the variables are of type
	portBASE_TYPE. */
	assert_user_read(_uxCurrentNumberOfTasks);

}

inline uxTaskPriorityGet()
	{
	//	atomic
	//	{
/*** change 5 ***/
			assert_user_atomic_read(_pxCurrentTCB);
/*** end 5 ***/
			assert_user_atomic_read(_uxPriority);         //variable inside tcb
	//	}
}

inline vTaskDelay()
{
		
	vTaskSuspendAll();

	
		interrupt();
		assert_user_read(_xTickCount);       // read common xTickCount 

		interrupt();
/*** change 5 ***/
		assert_user_read(_pxCurrentTCB);
		interrupt();
/*** end 5 ***/

		// Remove the generic list item ( generally from ready queue)
		assert_user_write(_pxReadyTasksLists);

/*** change 5 ***/
		interrupt();
		assert_user_read(_pxCurrentTCB);
/*** end 5 ***/		

		interrupt();
		assert_user_read(_xTickCount);       // read common xTickCount in IF statement

		// Insert into overflowed list for delayed list
		if
		:: skip ->
/*** change 5 ***/
			interrupt();
			assert_user_read(_pxCurrentTCB);
/*** end 5 ***/		

			interrupt();
			assert_user_write(_pxOverflowDelayedTaskList);

		:: skip -> 
/*** change 5 ***/
			interrupt();
			assert_user_read(_pxCurrentTCB);
/*** end 5 ***/		

			interrupt();
			assert_user_write(_pxDelayedTaskList);
		fi
	
	interrupt();

	xTaskResumeAll();

	interrupt();

	if
	:: xAlreadyYielded == 0 -> 	interrupt(); portYIELD_WITHIN_API(); // p1 or p2 
	:: else -> skip;
	fi
}

inline vTaskDelayUntil(  )
{

	vTaskSuspendAll();
		

		interrupt();
		assert_user_read(_xTickCount);       // read common xTickCount 
/*** change 5 ***/
		interrupt();
		assert_user_read(_pxCurrentTCB);
/*** end 5 ***/		

		interrupt();

		//remove current task from Generic list.
		assert_user_write(_pxReadyTasksLists);
/*** change 5 ***/
		interrupt();
		assert_user_read(_pxCurrentTCB);
/*** end 5 ***/		

		interrupt();
		assert_user_read(_xTickCount);       // read common xTickCount 

		if
		:: skip -> 
/*** change 5 ***/
			interrupt();
			assert_user_read(_pxCurrentTCB);
/*** end 5 ***/		
			
			interrupt();
			assert_user_write(_pxOverflowDelayedTaskList);

		:: skip ->			
/*** change 5 ***/
			interrupt();
			assert_user_read(_pxCurrentTCB);
/*** end 5 ***/		

			interrupt();
			assert_user_write(_pxDelayedTaskList);
		fi

	xTaskResumeAll();
			
	interrupt();

	if
	:: xAlreadyYielded == 0 -> 	interrupt(); portYIELD_WITHIN_API(); // p1 or p2 
	:: else -> skip;
	fi
}

inline vTaskDelete()
{

	interrupt();

//		atomic
	//	{
/*** change 5 ***/
	assert_user_atomic_read(_pxCurrentTCB);
/*** end 5 ***/		

			//remove current task from Generic list.
	if 
	:: skip -> assert_user_atomic_write(_pxReadyTasksLists);
	:: skip -> assert_user_atomic_write(_pxOverflowDelayedTaskList);
	:: skip -> assert_user_atomic_write(_pxDelayedTaskList);
	:: skip -> assert_user_atomic_write(_xSuspendedTaskList);
	fi;

	// remove from event List 
	if 
	:: skip -> assert_user_atomic_write(_xPendingReadyList);
	:: skip -> assert_user_atomic_write(_xTasksWaitingToSend);
	:: skip -> assert_user_atomic_write(_xTasksWaitingToReceive);
	:: skip -> skip;
	fi;

	//Insert into delete list 
	assert_user_atomic_write(_xTasksWaitingTermination);
	assert_user_atomic_write(_uxTasksDeleted);
	assert_user_atomic_write(_uxTaskNumber);	

		//}

	interrupt();

	if
	::schedulerRunning == 1 -> 	interrupt();
		if	// deleting current task
		:: skip -> portYIELD_WITHIN_API();
		:: skip -> skip;
		fi
	:: else -> skip;
	fi
}

inline vTaskIncrementTick() // called from within critical section or isr
{
/*** change 5 ***/
//	assert_user_atomic_read(_uxSchedulerSuspended);
/*** end 5 ***/
	
	if
	::SchedulerSuspended == 0 -> 	assert_user_atomic_write(_xTickCount);       // Write common xTickCount 

		// access to overflow and delayed list pointers 
		assert_user_atomic_read(_pxOverflowDelayedTaskList);
		assert_user_atomic_read(_pxDelayedTaskList);
		assert_user_atomic_read(_xNumOfOverflows);
		assert_user_atomic_read(_pxDelayedTaskList);
		assert_user_atomic_read(_xTickCount);       // read common xTickCount 
/*** change 5 ***/
		assert_user_atomic_read(_pxDelayedTaskList);
		//read generic list item inside prvCheckDelayedTask in if condition
		if 
		:: skip -> assert_user_atomic_read(_pxReadyTasksLists);
		:: skip -> assert_user_atomic_read(_pxOverflowDelayedTaskList);
		:: skip -> assert_user_atomic_read(_pxDelayedTaskList);
		:: skip -> assert_user_atomic_read(_xSuspendedTaskList);
		fi;
/*** end 5 ***/
		// remove generic list item
		if 
		:: skip -> assert_user_atomic_write(_pxReadyTasksLists);
		:: skip -> assert_user_atomic_write(_pxOverflowDelayedTaskList);
		:: skip -> assert_user_atomic_write(_pxDelayedTaskList);
		:: skip -> assert_user_atomic_write(_xSuspendedTaskList);
		fi;

		// read and remove from event List 
		if 
		:: skip -> assert_user_atomic_write(_xPendingReadyList);
		:: skip -> assert_user_atomic_write(_xTasksWaitingToSend);
		:: skip -> assert_user_atomic_write(_xTasksWaitingToReceive);
		:: skip -> skip;
		fi;

		// Add to ready queue;
		{
/*** change 5 ***/
			assert_user_atomic_read(_uxTopReadyPriority); // inside if condition
/*** end 5 ***/
			if
			// task has high priority
			:: skip -> assert_user_atomic_write(_uxTopReadyPriority);
			:: skip -> skip;
			fi
			assert_user_atomic_write(_pxReadyTasksLists);
		}
	:: else -> assert_user_atomic_write(_uxMissedTicks);
	fi
	
}	

inline vTaskMissedYield() // called inside critical section
{
	xMissedYield = 1;
}

inline vTaskPlaceOnEventList( q )
{

	interrupt();
	//insert into event list 
/*** change 5 ***/
	assert_user_read(_pxCurrentTCB); 
	interrupt();
/*** end 5 ***/
	assert_user_write(q);
		// remove generic list item
	/*** change 5 ***/
	assert_user_read(_pxCurrentTCB); 
	interrupt();
/*** end 5 ***/

	if 
	:: skip -> assert_user_write(_pxReadyTasksLists);
	:: skip -> assert_user_write(_pxOverflowDelayedTaskList);
	:: skip -> assert_user_write(_pxDelayedTaskList);
	:: skip -> assert_user_write(_xSuspendedTaskList);
	fi;

	if
		// wait indefinitely
	:: skip -> 
		interrupt();
		// Insert into suspended list 
/*** change 5 ***/
		assert_user_read(_pxCurrentTCB); 
		interrupt();
	/*** end 5 ***/
		assert_user_write(_xSuspendedTaskList);
	:: skip->
		
		interrupt();
		assert_user_read(_xTickCount);       // read common xTickCount 
	/*** change 5 ***/
		interrupt();
		assert_user_read(_pxCurrentTCB); 
		interrupt();
		assert_user_read(_xTickCount);       // read common xTickCount 
	/*** end 5 ***/
		
		if
		:: skip -> 
	/*** change 5 ***/
			interrupt();
			assert_user_read(_pxCurrentTCB); 
	/*** end 5 ***/
			interrupt();
			assert_user_write(_pxOverflowDelayedTaskList);
		:: skip -> 
	/*** change 5 ***/
			interrupt();
			assert_user_read(_pxCurrentTCB); 
	/*** end 5 ***/

			interrupt();
			assert_user_write(_pxDelayedTaskList);
		fi
	fi


}

inline vTaskPrioritySet()
{
	interrupt();
//	atomic
	//{

	assert_user_atomic_read(_pxCurrentTCB);

	assert_user_atomic_read(_uxPriority);         //variable inside tcb
	if
	:: skip ->
		assert_user_atomic_write(_uxPriority);         //variable inside tcb
		// Remove and add to ready list
		if 
		:: skip -> assert_user_atomic_write(_pxReadyTasksLists);
					{
	/*** change 5 ***/
						assert_user_atomic_read(_uxTopReadyPriority); // inside if condition
	/*** end 5 ***/

						if
						// task has high priority
						:: skip -> assert_user_atomic_write(_uxTopReadyPriority);
						:: skip -> skip;
						fi
						assert_user_atomic_write(_pxReadyTasksLists);
					}												
		:: skip -> skip;
		fi

		// yield if current priority is changed
		if
		:: skip -> portYIELD_WITHIN_API();
		:: skip -> skip
		fi

	:: skip -> skip;
	fi
//	}
	interrupt();

}

inline vTaskResume()
{
	interrupt();
	// accessed inside IF condition

	assert_user_read(_pxCurrentTCB);

	if
	:: skip ->
		interrupt();
	//	atomic
		//{
		if
				// remove from blocked list(suspend list) and add to ready list
		:: skip ->  assert_user_atomic_write(_xSuspendedTaskList);
	/*** change 5 ***/
			assert_user_atomic_read(_uxTopReadyPriority); // inside if condition
	/*** end 5 ***/

			if
			// task has high priority
			:: skip ->	assert_user_atomic_write(_uxTopReadyPriority);
			:: skip -> skip;
			fi
			assert_user_atomic_write(_pxReadyTasksLists);
			// higher priority task resumed
	/*** change 5 ***/
			assert_user_atomic_read(_pxCurrentTCB); 
	/*** end 5 ***/
			if
			:: skip -> portYIELD_WITHIN_API(); // p1 or p2 
			:: skip -> skip;
			fi
		:: skip -> skip;
		fi
		//}
		interrupt();
	:: skip -> skip;
	fi
}

inline vTaskSuspend()
{
	interrupt();
	//atomic
	//{
/*** change 5 ***/
	assert_user_atomic_read(_pxCurrentTCB); 
/*** end 5 ***/
		//remove current task from Generic list.
	if 
	:: skip -> assert_user_atomic_write(_pxReadyTasksLists);
	:: skip -> assert_user_atomic_write(_pxOverflowDelayedTaskList);
	:: skip -> assert_user_atomic_write(_pxDelayedTaskList);
	:: skip -> assert_user_atomic_write(_xSuspendedTaskList);
	fi;
	if 
	:: skip -> assert_user_atomic_write(_xPendingReadyList);
	:: skip -> assert_user_atomic_write(_xTasksWaitingToSend);
	:: skip -> assert_user_atomic_write(_xTasksWaitingToReceive);
	:: skip -> skip;
	fi;

// Add to suspend list 
	assert_user_atomic_write(_xSuspendedTaskList);
	//}

	interrupt();
	if
		// suspending current task 
	:: skip ->
		if 
		:: schedulerRunning == 1 ->	interrupt();
			portYIELD_WITHIN_API(); 
		:: else ->	interrupt();
			// access inside IF condition
			assert_user_read(_uxCurrentNumberOfTasks);
			if
			::uxCurrentNumberOfTasks == 1 -> 
				interrupt();
				assert_user_write(_pxCurrentTCB);
			:: else -> interrupt(); vTaskSwitchContext();        
			fi
		fi
	:: skip -> skip;
	fi
	
}

inline vTaskSuspendAll(){

	int temp;
	interrupt();
	temp =  SchedulerSuspended + 1;  // temp is used to validate that uxSchedulerSuspended will have values only 1 and 0;
	interrupt();
		SchedulerSuspended = temp;
	assert( SchedulerSuspended ==1);
	interrupt();

//	assert_user_write(_uxSchedulerSuspended);// IT IS SAID CRITICAL NOT NEEDED BECAUSE OF PORT BASE TYPE  
 


}	

inline vTaskSwitchContext()
{

	interrupt();	
	if
	:: SchedulerSuspended ==1 -> 	interrupt();
		
		xMissedYield = 1;
	:: else -> 	interrupt();
			// assign the current tcb to the highest priority task
			assert_user_read(_pxReadyTasksLists);
			interrupt();
	/*** change 5 ***/
			assert_user_read(_uxTopReadyPriority); 
	/*** end 5 ***/
			assert_user_write(_uxTopReadyPriority);
			interrupt();
			assert_user_write(_pxCurrentTCB);
			
	fi
}

inline vTaskSwitchContextISR()
{

	if
	:: SchedulerSuspended ==1 -> 
		
		xMissedYield = 1;
	:: else -> 
			// assign the current tcb to the highest priority task
			assert_user_atomic_read(_pxReadyTasksLists);
	/*** change 5 ***/
			assert_user_atomic_read(_uxTopReadyPriority); 
	/*** end 5 ***/
			assert_user_atomic_write(_uxTopReadyPriority);
			assert_user_atomic_write(_pxCurrentTCB);
			
	fi
}

inline xQueueCreate()
{

	skip;
}
// remove the content of queue delete code
inline vQueueDelete()
{
	skip;
	interrupt();
	vQueueUnregisterQueue();
	interrupt();
	//Fix added
	assert_user_atomic_write(_userQueue);

}

inline vQueueAddToRegistry()
{
	// The registry is always read
// Fixed the error foud in racing pair
//	assert_user_read(_xQueueRegistry);
	assert_user_atomic_read(_xQueueRegistry);
	interrupt();
	
	// The write to the registry may or may not occur (depending on whether there is free space in registry)
	if
	:: skip ->
// Fixed the error foud in racing pair
//		assert_user_read(_userQueue);	// read the pointer to the user queue
		assert_user_atomic_read(_userQueue);	// read the pointer to the user queue
//Fixed the error foud in racing pair
//		assert_user_write(_xQueueRegistry);	// write the queue to the registry
		assert_user_atomic_write(_xQueueRegistry);	// write the queue to the registry
	:: skip -> skip;
	fi
}

inline vQueueUnregisterQueue()
{
	// The pointer to the user queue is always read
// Fixed the error foud in racing pair
//	assert_user_read(_userQueue);
	assert_user_atomic_read(_userQueue);
	interrupt();
	// The write to the registry may or may not occur (depending on whether the queue is present in the registry)
	if
	:: skip ->
// Fixed the error foud in racing pair
//		assert_user_write(_xQueueRegistry);
		assert_user_atomic_write(_xQueueRegistry);
	:: skip -> skip;
	fi
}

inline xQueueIsQueueEmptyFromISR()
{
		// Read from a user queue 
		assert_user_atomic_read(_userQueue);
		assert_user_atomic_read(_uxMessagesWaiting);
}

inline xQueueIsQueueFullFromISR()
{
		// Read from a user queue 
		assert_user_atomic_read(_userQueue);
		assert_user_atomic_read(_uxMessagesWaiting);
}

inline xQueueReceive()
{
	interrupt();
	do 
	:: skip -> 
//		atomic
	//	{
				// is there any data in user queue in IF statement
			assert_user_atomic_read(_userQueue);
			assert_user_atomic_read(_uxMessagesWaiting);
			if
			// data in queue
			:: skip ->
				// Copy data from user queue
				assert_user_atomic_write(_userQueue);
				assert_user_atomic_write(_uxDataInQueue);
				if
				// just peeking false
				:: skip ->
					// Reduce queue message waiting by one
					assert_user_atomic_write(_userQueue);
					assert_user_atomic_write(_uxMessagesWaiting);
					// Access in IF statement 
					assert_user_atomic_read(_xTasksWaitingToSend);
					if
					:: skip ->
						xTaskRemoveFromEventList( _xTasksWaitingToSend );
						if
						// higher pirority task woken up
						:: skip ->
							portYIELD_WITHIN_API();
						:: skip -> skip ;
						fi
					:: skip -> skip;
					fi
				:: skip ->
					if
						// task waiting to receive 
					:: skip ->
						// reset the read pointer of queue
						assert_user_atomic_write(_userQueue);
						assert_user_atomic_read(_xTasksWaitingToReceive);
						xTaskRemoveFromEventList( _xTasksWaitingToReceive )
						if // higher priority task woken up
						:: skip -> portYIELD_WITHIN_API(); // p1 or p2 
						:: skip -> skip;
						fi
					:: skip -> skip;
					fi
				fi
				break;
			:: skip ->
				if
				// queue is empty and no block time specified
				:: skip -> 
							break;
				:: skip -> skip;
				fi
			fi
		//}

		vTaskSuspendAll();
		prvLockQueue();

		if
		// the timeout has not exipred
		:: skip ->
	/*** change 5 ***/
			interrupt();
			assert_user_atomic_read(_userQueue); // inside if condition
			interrupt();
	/*** end 5 ***/
			assert_user_atomic_read(_uxMessagesWaiting); // inside if condition
			if
				// Queue is empty
			:: skip ->
				vTaskPlaceOnEventList( _xTasksWaitingToReceive);          
				prvUnlockQueue(  );
				xTaskResumeAll();
				 interrupt();
				if
				:: xAlreadyYielded == 0 -> 	interrupt(); portYIELD_WITHIN_API(); // p1 or p2 
				:: else -> skip;
				fi
			:: skip ->
				prvUnlockQueue(  );
				xTaskResumeAll();
			fi
		:: skip ->
			prvUnlockQueue(  );
			xTaskResumeAll();
			break;
		fi
	od
}

inline xQueueReceiveFromISR()
{
		// Read from a user queue inside IF condition
		assert_user_atomic_read(_userQueue);
		assert_user_atomic_read(_uxMessagesWaiting);
		if
		// message present in queue
		:: skip -> 
			// copy data from queue 
			assert_user_atomic_write(_userQueue);
			assert_user_atomic_write(_uxDataInQueue);
			assert_user_atomic_write(_uxMessagesWaiting);
			
			/* If the queue is locked we will not modify the event list.  Instead
			we update the lock count so the task that unlocks the queue will know
			that an ISR has removed data while the queue was locked. */
			if
			:: xRx == 0 -> assert_user_atomic_write(_xTasksWaitingToSend);
				if
				// queue waiting to send not empty
				:: skip ->
						xTaskRemoveFromEventList(_xTasksWaitingToSend);
					if
					// higher priority task woken
					:: skip ->
						pxTaskWoken = 1
					:: skip -> skip;
					fi
				:: skip-> skip;
				fi
			
			:: else ->
				/* Increment the lock count so the task that unlocks the queue
				knows that data was removed while it was locked. */
	/*** change 5 ***/
				assert_user_atomic_write(_userQueue); // inside if condition
	/*** end 5 ***/
				xRx = xRx + 1;
			fi
		:: skip -> skip
		fi
}

inline xQueueSend()
{
	interrupt();
	do 
	:: skip -> 
		//atomic
		//{
				// is there any data in user queue in IF statement
			assert_user_atomic_read(_userQueue);
			assert_user_atomic_read(_uxMessagesWaiting);
			if
			// data in queue
			:: skip ->
				// Copy data to user queue
				assert_user_atomic_write(_userQueue);
				assert_user_atomic_write(_uxDataInQueue);
				assert_user_atomic_write(_uxMessagesWaiting);
				
				// Access in IF statement 
				assert_user_atomic_read(_xTasksWaitingToReceive);
				if
				:: skip ->
					xTaskRemoveFromEventList( _xTasksWaitingToReceive );
					if
					// higher pirority task woken up
					:: skip ->
						portYIELD_WITHIN_API();
					:: skip -> skip ;
					fi
				:: skip -> skip;
				fi
				break;
			:: skip ->
				if
				// queue is empty and no block time specified
				:: skip -> 
							break;
				:: skip -> skip;
				fi
			fi
		//}

		vTaskSuspendAll();
		prvLockQueue();

		if
		// the timeout has not exipred
		:: skip ->
	/*** change 5 ***/
			interrupt();
			assert_user_atomic_read(_userQueue); // inside if condition
			interrupt();
	/*** end 5 ***/
			assert_user_atomic_read(_uxMessagesWaiting); // inside if condition

			if
				// Queue is full
			:: skip ->
				vTaskPlaceOnEventList( _xTasksWaitingToSend);          
				prvUnlockQueue(  );
				xTaskResumeAll();
				interrupt();
				if
				:: xAlreadyYielded == 0 ->	interrupt(); portYIELD_WITHIN_API(); // p1 or p2 
				:: else -> skip;
				fi
			:: skip ->
				prvUnlockQueue(  );
				xTaskResumeAll();
			fi
		:: skip ->
			prvUnlockQueue(  );
			xTaskResumeAll();
			break;
		fi
	od
}

inline xQueueSendFromISR()
{
			// Read from a user queue inside IF condition
		assert_user_atomic_read(_userQueue);
		assert_user_atomic_read(_uxMessagesWaiting);
		if
		// Queue is not full
		:: skip -> 
				// write data into queue
			assert_user_atomic_write(_userQueue);
			assert_user_atomic_write(_uxDataInQueue);
			assert_user_atomic_write(_uxMessagesWaiting);
	/*** change 5 ***/
			assert_user_atomic_read(_userQueue); //inside if statement
	/*** end 5 ***/
			if
			:: xTx == 0 ->	assert_user_atomic_write(_xTasksWaitingToReceive);	
				if
				// queue waiting to receive not empty
				:: skip ->
						xTaskRemoveFromEventList(_xTasksWaitingToReceive);
					if
					// higher priority task woken
					:: skip ->
						pxTaskWoken = 1
					:: skip -> skip;
					fi
				:: skip-> skip;
				fi
			
			:: else ->
				/* Increment the lock count so the task that unlocks the queue
				knows that data was posted while it was locked. */
	/*** change 5 ***/
				assert_user_atomic_write(_userQueue); //inside if statement
	/*** end 5 ***/
				xTx = xTx + 1;
			fi
		:: skip -> skip
		fi
}

inline xTaskCreate()
{

// code for Allocation of memory for task TCB and stack 
// code for Initialization the TCB
// code because they dont affect any shared variable
	interrupt();
//	atomic{
		uxCurrentNumberOfTasks = uxCurrentNumberOfTasks + 1;
		assert_user_atomic_write(_uxCurrentNumberOfTasks);
	/*** change 5 ***/

		assert_user_atomic_read(_pxCurrentTCB); // inside if condition

/* There are no other tasks, or all the other tasks are in
				the suspended state - make this the current task. */

		if 	//pxCurrentTCB is null ( if error found in 'pxCurrentTCB' in this section, it may be false positive)
		:: skip ->	assert_user_atomic_write(_pxCurrentTCB);
				assert_user_atomic_read(_uxCurrentNumberOfTasks); // inside if condition
				if	// First task 
				:: skip -> // initialize all the queues
							assert_user_atomic_write(_pxReadyTasksLists);
							assert_user_atomic_write(_pxDelayedTaskList);
							assert_user_atomic_write(_pxOverflowDelayedTaskList);
							assert_user_atomic_write(_xPendingReadyList);
							assert_user_atomic_write(_xTasksWaitingTermination);
							assert_user_atomic_write(_xSuspendedTaskList);
				:: skip -> skip;
				fi
		:: else ->
	/*** end 5 ***/
			if
			:: schedulerRunning == 0 ->
		/*** change 5 ***/
					assert_user_atomic_read(_pxCurrentTCB); // inside if condition
		/*** end 5 ***/
					if 			// new priority higher than current tcb
					:: skip ->		assert_user_atomic_write(_pxCurrentTCB);
					:: skip -> skip;
					fi
			:: else -> skip;
			fi		
	/*** change 5 ***/
		fi		
		assert_user_atomic_read(_uxTopUsedPriority); // inside if condition
	/*** end 5 ***/
		if  // new priority higher than current tcb
		:: skip -> assert_user_atomic_write(_uxTopUsedPriority);
		:: skip -> skip;
		fi
		assert_user_atomic_write(_uxTaskNumber);

		//Add to ready queue
		{
	/*** change 5 ***/
			assert_user_atomic_read(_uxTopUsedPriority); // inside if condition
	/*** end 5 ***/
	
			if
			// task has high priority
			:: skip ->
				assert_user_atomic_write(_uxTopReadyPriority);
			:: skip -> skip;
			fi
			assert_user_atomic_write(_pxReadyTasksLists);
		}
	//}
	interrupt();
	if
	::schedulerRunning == 1 ->	interrupt();
		// Access inside if statement
		assert_user_read(_uxPriority);         //variable inside tcb
		interrupt();		
		
		// !!! FIX !!!
		assert_user_atomic_read(_pxCurrentTCB);
		interrupt();																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																			
		if	// new priority higher than current task
		:: skip -> portYIELD_WITHIN_API();
		:: skip -> skip;
		fi
	:: else -> skip;
	fi																																
}

inline xTaskGetTickCount()
{
	interrupt();
///	atomic
	//{
		assert_user_atomic_read(_xTickCount);// read common xTickCount 
	//}
}

inline xTaskGetTickCountFromISR()
{
	assert_user_atomic_read(_xTickCount);// read common xTickCount 
}

inline xTaskRemoveFromEventList(q) // called either from a critical section or from ISR function
{
	assert_user_atomic_write(q);

	if
	:: SchedulerSuspended == 0 ->
	// Remove genric list and add it to ready queue	
		if 
		:: skip -> assert_user_atomic_write(_pxReadyTasksLists);
		:: skip -> assert_user_atomic_write(_pxOverflowDelayedTaskList);
		:: skip -> assert_user_atomic_write(_pxDelayedTaskList);
		:: skip -> assert_user_atomic_write(_xSuspendedTaskList);
		fi;

			// add to ready queue
			{
	/*** change 5 ***/
				assert_user_atomic_read(_uxTopReadyPriority); // inside if condition
	/*** end 5 ***/	
				if
				// task has high priority
				:: skip ->	assert_user_atomic_write(_uxTopReadyPriority);
				:: skip -> skip;
				fi
				assert_user_atomic_write(_pxReadyTasksLists);
			}
	:: else ->
		// Add to pending ready list
		assert_user_atomic_write(_xPendingReadyList);
	fi
/*** change 5 ***/
	assert_user_atomic_read(_pxCurrentTCB); // inside if condition
/*** end 5 ***/

}

inline xTaskResumeAll(){
	int temp;
	interrupt();
//atomic {
	temp = SchedulerSuspended - 1 ;		// Common global variable //temp is used to show uxSchedulerSuspended will have value 0 or 1 only
	SchedulerSuspended = temp;
	assert(SchedulerSuspended ==0);
/*** change 5 ***/
//	assert_user_atomic_write(_uxSchedulerSuspended);
//	assert_user_atomic_read(_uxSchedulerSuspended); // inside if condition
/*** end 5 ***/

	if
	:: SchedulerSuspended == 0 ->
			// Access inside if statement
			assert_user_atomic_read(_uxCurrentNumberOfTasks);
			assert_user_atomic_write(_xPendingReadyList); // read and remove from xPendingReadyList and remove from generic list; 
			if 
			:: skip -> assert_user_atomic_write(_pxReadyTasksLists);
			:: skip -> assert_user_atomic_write(_pxOverflowDelayedTaskList);
			:: skip -> assert_user_atomic_write(_pxDelayedTaskList);
			:: skip -> assert_user_atomic_write(_xSuspendedTaskList);
			fi;

			// Add to ready queue;
			{
	/*** change 5 ***/
				assert_user_atomic_read(_uxTopReadyPriority); // inside if condition
	/*** end 5 ***/
				if
				// task has high priority
				:: skip ->	assert_user_atomic_write(_uxTopReadyPriority);
				:: skip -> skip;
				fi
				assert_user_atomic_write(_pxReadyTasksLists);
			}
	/*** change 5 ***/
			assert_user_atomic_read(_pxCurrentTCB); // inside if condition
	/*** end 5 ***/
			// Access inside if statement		
			assert_user_atomic_read(_uxPriority);        //variable inside tcb

			if
			::  skip ->	assert_user_atomic_write(_uxMissedTicks);
									vTaskIncrementTick();               // call to increment tick
									assert_user_atomic_write(_uxMissedTicks);// common variable
			:: skip -> skip;
			fi

			if
			:: skip -> 
				xAlreadyYielded = 1;
				xMissedYield = 0;
				portYIELD_WITHIN_API();
			:: skip -> skip;
			fi
	:: else -> skip
	fi
//} // end of atomic // critical section  is treated as atomic 
	interrupt();
}

inline xTaskResumeFromISR()
{
		if
		// task is suspended
		::skip -> 

	/*** change 5 ***/
//			assert_user_atomic_read(_uxSchedulerSuspended); // inside if condition
	/*** end 5 ***/
			if
			:: SchedulerSuspended == 0 ->
	/*** change 5 ***/
				assert_user_atomic_read(_pxCurrentTCB); // result for xYieldRequired
	/*** end 5 ***/

				// Remove from suspend queue and add to ready queue
				assert_user_atomic_write(_xSuspendedTaskList);
				//add task to ready list
				{				
	/*** change 5 ***/
					assert_user_atomic_read(_uxTopReadyPriority); // inside if condition
	/*** end 5 ***/

					if
					// task has high priority
					:: skip ->	assert_user_atomic_write(_uxTopReadyPriority);
					:: skip -> skip;
					fi
					assert_user_atomic_write(_pxReadyTasksLists);
				}
			::else ->	assert_user_atomic_write(_xPendingReadyList);// Add to pending ready list
			fi
		:: skip->skip;
		fi
}

proctype isr(){

	bit pxTaskWoken=0;
	do
	:: skip -> getLock(isr_lc);xTaskGetTickCountFromISR();choose();
	:: skip -> getLock(isr_lc);xTaskResumeFromISR();choose();
	:: skip -> getLock(isr_lc);xQueueIsQueueEmptyFromISR();choose();
	:: skip -> getLock(isr_lc);xQueueIsQueueFullFromISR();choose();
	:: skip -> getLock(isr_lc);uxQueueMessagesWaitingFromISR();choose();
	:: skip -> getLock(isr_lc);pxTaskWoken=0;xQueueReceiveFromISR();choose();// if pxtaskwoken =1 after the function then context switch is required
	:: skip -> getLock(isr_lc);pxTaskWoken=0;xQueueSendFromISR();choose();// if pxtaskwoken =1 after the function then context switch is required
	:: skip -> getLock(isr_lc);vTaskIncrementTick(); vTaskSwitchContextISR(); choose();// context switch may happen after the task switch context // Tick interrupt
	od
}

proctype p1(){

	bit xAlreadyYielded =0;

	do
	:: skip -> getLock(api_lc);xAlreadyYielded =0; vTaskDelay();releaseLock(sh_lc);  
	:: skip -> getLock(api_lc);xAlreadyYielded =0; vTaskDelayUntil();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);vTaskDelete();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);xTaskCreate();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);xTaskGetTickCount();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);uxTaskGetNumberOfTasks();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);uxTaskPriorityGet();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);vTaskPrioritySet();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);vTaskResume();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);vTaskSuspend();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);xQueueCreate();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);vQueueDelete();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);uxQueueMessagesWaiting();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);xAlreadyYielded =0;xQueueReceive();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);xAlreadyYielded =0;xQueueSend();releaseLock(sh_lc);
	od
}

proctype p2(){

	bit xAlreadyYielded = 0;

	do
	:: skip -> getLock(api_lc);xAlreadyYielded = 0; vTaskDelay();releaseLock(sh_lc);  
	:: skip -> getLock(api_lc);xAlreadyYielded = 0; vTaskDelayUntil();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);vTaskDelete();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);xTaskCreate();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);xTaskGetTickCount();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);uxTaskGetNumberOfTasks();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);uxTaskPriorityGet();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);vTaskPrioritySet();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);vTaskResume();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);vTaskSuspend();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);xQueueCreate();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);vQueueDelete();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);uxQueueMessagesWaiting();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);xAlreadyYielded = 0;xQueueReceive();releaseLock(sh_lc);
	:: skip -> getLock(api_lc);xAlreadyYielded = 0;xQueueSend();releaseLock(sh_lc);
	od
}

init{
	schedulerRunning = 1;
	run scheduler();
	run p1();	//API 1	
	run p2();	//API 2
	run isr();
}